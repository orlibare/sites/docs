---
title: "Préparatifs"
weight: 1
description: "Avant de commencer : préparation du socle d'installation."
---

## Déroulement

### Création de la machine virtuelle

```
vmId=53216
vmName="dev-lfs"
vmMacAddress="12:06:59:05:32:16"
```
```
sudo qm create $vmId \
--scsihw virtio-scsi-single \
--scsi0 local-lvm:80 \
--efidisk0 local-lvm:4 \
--sockets 1 \
--cores 8 \
--ostype l26 \
--bios ovmf \
--machine q35 \
--memory 8192 \
--name $vmName \
--net0 virtio=$vmMacAddress,bridge=vmbr3,firewall=0 \
--numa 1 \
--pool Test \
--tablet 0 \
--vga qxl \
--ide0 local:iso/debian-live-12.5.0-amd64-xfce.iso,media=cdrom \
--boot order='ide0;scsi0' \
--agent 0 \
--onboot 0
```
```
sudo qm start $vmId
```



### Connexion locale à la machine virtuelle

```
sudo apt -y install htop   \
                    screen \
                    ssh    \
                    tree   \
                    wget
```

On récupère l'adresse IP via `hostname -I`.

### Connexion distante à la machine virtuelle

On se connecte via SSH avec `ssh user@<adresse IP>` avec le mot passe __live__. Une fois connecté, on exécute la commande `screen`.

### Script des pré-requis

```
cat > version-check.sh << "EOF"
#!/bin/bash
# Un script qui liste les numéro de version des outils de développement critiques

# Si vous avez des outils installés dans d'autres répertoires, ajustez PATH ici ET
# dans ~lfs/.bashrc (section 4.4) également.

LC_ALL=C 
PATH=/usr/bin:/bin

bail() { echo "FATAL : $1"; exit 1; }
grep --version > /dev/null 2> /dev/null || bail "grep ne fonctionne pas"
sed '' /dev/null || bail "sed ne fonctionne pas"
sort   /dev/null || bail "sort ne fonctionne pas"

ver_check()
{
   if ! type -p $2 &>/dev/null
   then 
     echo "ERREUR : $2 ($1) introuvable"; return 1; 
   fi
   v=$($2 --version 2>&1 | grep -E -o '[0-9]+\.[0-9\.]+[a-z]*' | head -n1)
   if printf '%s\n' $3 $v | sort --version-sort --check &>/dev/null
   then 
     printf "OK :     %-9s %-6s >= $3\n" "$1" "$v"; return 0;
   else 
     printf "ERREUR : %-9s est TROP VIEUX (version $3 ou supérieure requise)\n" "$1"; 
     return 1; 
   fi
}

ver_kernel()
{
   kver=$(uname -r | grep -E -o '^[0-9\.]+')
   if printf '%s\n' $1 $kver | sort --version-sort --check &>/dev/null
   then 
     printf "OK :     noyau Linux $kver >= $1\n"; return 0;
   else 
     printf "ERREUR : noyau Linux ($kver) est TROP VIEUX (version $1 ou supérieure requise)\n" "$kver"; 
     return 1; 
   fi
}

# Coreutils en premier car --version-sort a besoin de Coreutils >= 7.0
ver_check Coreutils      sort     8.1 || bail "Coreutils trop vieux, arrêt"
ver_check Bash           bash     3.2
ver_check Binutils       ld       2.13.1
ver_check Bison          bison    2.7
ver_check Diffutils      diff     2.8.1
ver_check Findutils      find     4.2.31
ver_check Gawk           gawk     4.0.1
ver_check GCC            gcc      5.2
ver_check "GCC (C++)"    g++      5.2
ver_check Grep           grep     2.5.1a
ver_check Gzip           gzip     1.3.12
ver_check M4             m4       1.4.10
ver_check Make           make     4.0
ver_check Patch          patch    2.5.4
ver_check Perl           perl     5.8.8
ver_check Python         python3  3.4
ver_check Sed            sed      4.1.5
ver_check Tar            tar      1.22
ver_check Texinfo        texi2any 5.0
ver_check Xz             xz       5.0.0
ver_kernel 4.19

if mount | grep -q 'devpts on /dev/pts' && [ -e /dev/ptmx ]
then echo "OK :     le noyau Linux prend en charge les PTY UNIX 98";
else echo "ERREUR : le noyau Linux ne prend PAS en charge les PTY UNIX 98"; fi

alias_check() {
   if $1 --version 2>&1 | grep -qi $2
   then printf "OK :     %-4s est $2\n" "$1";
   else printf "ERREUR : %-4s n'est PAS $2\n" "$1"; fi
}
echo "Alias :"
alias_check awk GNU
alias_check yacc Bison
alias_check sh Bash

echo "Vérification du compilateur :"
if printf "int main(){}" | g++ -x c++ -
then echo "OK :     g++ fonctionne";
else echo "ERREUR : g++ ne fonctionne PAS"; fi
rm -f a.out

if [ "$(nproc)" = "" ]; then
   echo "ERREUR : nproc n'est pas disponible ou a produit une sortie vide"
else
   echo "OK : nproc rapporte $(nproc) cœurs logiques disponibles"
fi
EOF
```



### Test des pré-requis

```
bash version-check.sh
```

```
OK :     Coreutils 9.1    >= 8.1
OK :     Bash      5.2.15 >= 3.2
OK :     Binutils  2.40   >= 2.13.1
ERREUR : bison (Bison) introuvable
OK :     Diffutils 3.8    >= 2.8.1
OK :     Findutils 4.9.0  >= 4.2.31
ERREUR : gawk (Gawk) introuvable
OK :     GCC       12.2.0 >= 5.2
OK :     GCC (C++) 12.2.0 >= 5.2
OK :     Grep      3.8    >= 2.5.1a
OK :     Gzip      1.12   >= 1.3.12
ERREUR : m4 (M4) introuvable
OK :     Make      4.3    >= 4.0
OK :     Patch     2.7.6  >= 2.5.4
OK :     Perl      5.36.0 >= 5.8.8
OK :     Python    3.11.2 >= 3.4
OK :     Sed       4.9    >= 4.1.5
OK :     Tar       1.34   >= 1.22
ERREUR : texi2any (Texinfo) introuvable
OK :     Xz        5.4.1  >= 5.0.0
OK :     noyau Linux 6.1.0 >= 4.19
OK :     le noyau Linux prend en charge les PTY UNIX 98
Alias :
ERREUR : awk  n'est PAS GNU
ERREUR : yacc n'est PAS Bison
ERREUR : sh   n'est PAS Bash
Vérification du compilateur :
OK :     g++ fonctionne
OK : nproc rapporte 8 cœurs logiques disponibles
```

### Installation des pré-requis

```
sudo apt install -y bison   \
                    gawk    \
                    m4      \
                    texinfo
```

```
sudo ln -sf /bin/bash /bin/sh
```

### Validation des pré-requis

```
bash version-check.sh
```

```
OK :     Coreutils 9.1    >= 8.1
OK :     Bash      5.2.15 >= 3.2
OK :     Binutils  2.40   >= 2.13.1
OK :     Bison     3.8.2  >= 2.7
OK :     Diffutils 3.8    >= 2.8.1
OK :     Findutils 4.9.0  >= 4.2.31
OK :     Gawk      5.2.1  >= 4.0.1
OK :     GCC       12.2.0 >= 5.2
OK :     GCC (C++) 12.2.0 >= 5.2
OK :     Grep      3.8    >= 2.5.1a
OK :     Gzip      1.12   >= 1.3.12
OK :     M4        1.4.19 >= 1.4.10
OK :     Make      4.3    >= 4.0
OK :     Patch     2.7.6  >= 2.5.4
OK :     Perl      5.36.0 >= 5.8.8
OK :     Python    3.11.2 >= 3.4
OK :     Sed       4.9    >= 4.1.5
OK :     Tar       1.34   >= 1.22
OK :     Texinfo   6.8    >= 5.0
OK :     Xz        5.4.1  >= 5.0.0
OK :     noyau Linux 6.1.0 >= 4.19
OK :     le noyau Linux prend en charge les PTY UNIX 98
Alias :
OK :     awk  est GNU
OK :     yacc est Bison
OK :     sh   est Bash
Vérification du compilateur :
OK :     g++ fonctionne
OK : nproc rapporte 8 cœurs logiques disponibles
```

Là, on est bien !

## Vocabulaire

