---
title: "Paquets source"
weight: 3
description: "Téléchargement des paquets et correctifs nécessaires à la construction du système."
---

## Déroulement

### Création du répertoire des sources

```
sudo mkdir -v $LFS/sources
sudo chmod -v a+wt $LFS/sources
```
### Téléchargement des sources

```
wget http://fr.linuxfromscratch.org/view/lfs-systemd-stable/wget-list-systemd
wget --input-file=wget-list-systemd --continue --directory-prefix=$LFS/sources
```
### Vérification des sources

```
wget http://fr.linuxfromscratch.org/view/lfs-systemd-stable/md5sums
```
```
pushd $LFS/sources
    md5sum -c ~/md5sums --quiet
popd
```

```
md5sum: expat-2.6.0.tar.xz: No such file or directory
expat-2.6.0.tar.xz: FAILED open or read
md5sum: WARNING: 1 listed file could not be read
```

On remarque que le paquet expat est manquant. Une infomation sur le site indique qu'il faut choisir la version 2.6.2 en lien de la version 2.6.0. On modifie le fichier des sources et et on relance le téléchargement.

```
cat wget-list-systemd | grep expat
sed -i "s/expat-2.6.0.tar.xz/expat-2.6.2.tar.xz/g" wget-list-systemd
wget --input-file=wget-list-systemd --continue --directory-prefix=$LFS/sources
```

On s'occupe également du fichier md5sum. 

```
md5sum $LFS/sources/expat-2.6.2.tar.xz
```

Cela donne une valeur que l'on compare à celle du site de téléchargement. On modifie notre fichier md5sum et on relance la vérification globale.

```

pushd $LFS/sources
    sed -i "/.expat-2.6.0.tar.xz/d" ~/md5sums
    md5sum expat-2.6.2.tar.xz >> ~/md5sums
    md5sum -c ~/md5sums --quiet
popd
```



### Ajustement des droits

```
sudo chown root:root $LFS/sources/*
```

```
ls -lisa $LFS/sources/
```

```
1441830    264 -rw-r--r-- 1 root root    268261 Jan 10 23:12 Jinja2-3.1.3.tar.gz
1441844     20 -rw-r--r-- 1 root root     19384 Feb  2 16:31 MarkupSafe-2.1.5.tar.gz
1441856  20112 -rw-r--r-- 1 root root  20591308 Feb  6 21:01 Python-3.12.2.tar.xz
1441872    276 -rw-r--r-- 1 root root    279029 Dec 29 00:31 XML-Parser-2.47.tar.gz
1441794    364 -rw-r--r-- 1 root root    371680 Jan 24 05:01 acl-2.3.2.tar.xz
1441795    484 -rw-r--r-- 1 root root    492539 Jan 14 02:52 attr-2.5.2.tar.gz
1441796   1360 -rw-r--r-- 1 root root   1389680 Dec 22 19:13 autoconf-2.72.tar.xz
1441797   1568 -rw-r--r-- 1 root root   1601740 Oct  4  2021 automake-1.16.5.tar.xz
1441876      8 -rw-r--r-- 1 root root      5944 Feb 29 15:35 bash-5.2.21-upstream_fixes-1.patch
1441798  10696 -rw-r--r-- 1 root root  10952391 Nov  9 21:30 bash-5.2.21.tar.gz
1441799    460 -rw-r--r-- 1 root root    468572 Jan  4 17:53 bc-6.7.5.tar.xz
1441800  26924 -rw-r--r-- 1 root root  27567160 Jan 29 15:25 binutils-2.42.tar.xz
1441801   2752 -rw-r--r-- 1 root root   2817324 Sep 25  2021 bison-3.8.2.tar.xz
1441877      4 -rw-r--r-- 1 root root      1684 Feb 29 15:35 bzip2-1.0.8-install_docs-1.patch
1441802    792 -rw-r--r-- 1 root root    810029 Jul 13  2019 bzip2-1.0.8.tar.gz
1441803    760 -rw-r--r-- 1 root root    774985 Dec  8  2021 check-0.15.2.tar.gz
1441878    168 -rw-r--r-- 1 root root    169617 Feb 29 15:35 coreutils-9.4-i18n-1.patch
1441804   5840 -rw-r--r-- 1 root root   5979200 Aug 29  2023 coreutils-9.4.tar.xz
1441805   1344 -rw-r--r-- 1 root root   1372328 Sep  1  2023 dbus-1.14.10.tar.xz
1441806    608 -rw-r--r-- 1 root root    622059 Jun 17  2021 dejagnu-1.6.3.tar.gz
1441807   1588 -rw-r--r-- 1 root root   1624240 May 21  2023 diffutils-3.10.tar.xz
1441808   9412 -rw-r--r-- 1 root root   9637717 Feb  7  2023 e2fsprogs-1.47.0.tar.gz
1441809   8952 -rw-r--r-- 1 root root   9162766 Nov  3 17:23 elfutils-0.190.tar.bz2
1441883    476 -rw-r--r-- 1 root root    485236 Mar 13 19:19 expat-2.6.2.tar.xz
1441810    620 -rw-r--r-- 1 root root    632363 Feb  4  2018 expect5.45.4.tar.gz
1441811   1220 -rw-r--r-- 1 root root   1246503 Jul 27  2023 file-5.45.tar.gz
1441812   2000 -rw-r--r-- 1 root root   2046252 Feb  2  2022 findutils-4.9.0.tar.xz
1441813   1388 -rw-r--r-- 1 root root   1419096 Dec  7  2021 flex-2.6.4.tar.gz
1441814     44 -rw-r--r-- 1 root root     41917 May 14  2023 flit_core-3.9.0.tar.gz
1441815   3356 -rw-r--r-- 1 root root   3436180 Nov  2 13:48 gawk-5.3.0.tar.xz
1441816  85800 -rw-r--r-- 1 root root  87858592 Jul 27  2023 gcc-13.2.0.tar.xz
1441817   1092 -rw-r--r-- 1 root root   1115854 Feb  4  2022 gdbm-1.23.tar.gz
1441818  10016 -rw-r--r-- 1 root root  10255384 Nov 19 21:12 gettext-0.22.4.tar.xz
1441879      4 -rw-r--r-- 1 root root      2804 Feb 29 15:35 glibc-2.39-fhs-1.patch
1441819  18088 -rw-r--r-- 1 root root  18520988 Jan 31 22:06 glibc-2.39.tar.xz
1441820   2048 -rw-r--r-- 1 root root   2094196 Jul 30  2023 gmp-6.3.0.tar.xz
1441821   1188 -rw-r--r-- 1 root root   1215925 Jan  5  2017 gperf-3.1.tar.gz
1441822   1664 -rw-r--r-- 1 root root   1703776 May 13  2023 grep-3.11.tar.xz
1441823   7260 -rw-r--r-- 1 root root   7433031 Jul  5  2023 groff-1.23.0.tar.gz
1441824   6520 -rw-r--r-- 1 root root   6675608 Dec 20 16:53 grub-2.12.tar.xz
1441825    820 -rw-r--r-- 1 root root    838248 Aug 20  2023 gzip-1.13.tar.xz
1441826    592 -rw-r--r-- 1 root root    602747 Jan 28 02:56 iana-etc-20240125.tar.gz
1441827   1632 -rw-r--r-- 1 root root   1670424 Dec 29 18:17 inetutils-2.5.tar.xz
1441828    160 -rw-r--r-- 1 root root    162286 Mar  9  2015 intltool-0.51.0.tar.gz
1441829    900 -rw-r--r-- 1 root root    919800 Jan  8 17:39 iproute2-6.7.0.tar.xz
1441880     16 -rw-r--r-- 1 root root     12640 Feb 29 15:35 kbd-2.6.4-backspace-1.patch
1441831   1472 -rw-r--r-- 1 root root   1504584 Dec 11 14:04 kbd-2.6.4.tar.xz
1441832    560 -rw-r--r-- 1 root root    570936 Sep 29  2023 kmod-31.tar.xz
1441833    580 -rw-r--r-- 1 root root    592291 Aug 12  2023 less-643.tar.gz
1441834    188 -rw-r--r-- 1 root root    189200 May 15  2023 libcap-2.69.tar.xz
1441835   1332 -rw-r--r-- 1 root root   1362394 Oct 24  2022 libffi-3.4.4.tar.gz
1441836    956 -rw-r--r-- 1 root root    977735 Nov 13  2022 libpipeline-1.5.7.tar.gz
1441837    996 -rw-r--r-- 1 root root   1016040 Mar 17  2022 libtool-2.4.7.tar.xz
1441838    612 -rw-r--r-- 1 root root    624112 Jul  5  2023 libxcrypt-4.4.36.tar.xz
1441839 138132 -rw-r--r-- 1 root root 141444608 Feb  5 20:28 linux-6.7.4.tar.xz
1441840   1620 -rw-r--r-- 1 root root   1654908 May 28  2021 m4-1.4.19.tar.xz
1441841   2296 -rw-r--r-- 1 root root   2348200 Feb 26  2023 make-4.4.1.tar.gz
1441842   1944 -rw-r--r-- 1 root root   1987444 Sep 23  2023 man-db-2.12.0.tar.xz
1441843   2116 -rw-r--r-- 1 root root   2166012 Feb 12 01:24 man-pages-6.06.tar.xz
1441845   2172 -rw-r--r-- 1 root root   2223798 Feb 13 20:11 meson-1.3.2.tar.gz
1441846    756 -rw-r--r-- 1 root root    773573 Dec 16  2022 mpc-1.3.1.tar.gz
1441847   1460 -rw-r--r-- 1 root root   1493608 Aug 22  2023 mpfr-4.2.1.tar.xz
1441848   2152 -rw-r--r-- 1 root root   2201780 Jan 19 04:20 ncurses-6.4-20230520.tar.xz
1441849    228 -rw-r--r-- 1 root root    229479 Apr 10 07:43 ninja-1.11.1.tar.gz
1441850  17320 -rw-r--r-- 1 root root  17733249 Jan 30 14:48 openssl-3.2.1.tar.gz
1441851    768 -rw-r--r-- 1 root root    783756 Feb  6  2018 patch-2.7.6.tar.xz
1441852  13360 -rw-r--r-- 1 root root  13679524 Nov 29 16:10 perl-5.38.2.tar.xz
1441853    308 -rw-r--r-- 1 root root    311956 Feb  4 11:41 pkgconf-2.1.1.tar.xz
1441854   1372 -rw-r--r-- 1 root root   1401540 Aug 31  2023 procps-ng-4.0.4.tar.xz
1441855    416 -rw-r--r-- 1 root root    424736 Dec 13  2022 psmisc-23.6.tar.xz
1441857   8068 -rw-r--r-- 1 root root   8257814 Feb  6 21:01 python-3.12.2-docs-html.tar.bz2
1441881     16 -rw-r--r-- 1 root root     12418 Feb 29 15:35 readline-8.2-upstream_fixes-3.patch
1441858   2976 -rw-r--r-- 1 root root   3043952 Sep 26  2022 readline-8.2.tar.gz
1441859   1368 -rw-r--r-- 1 root root   1397092 Nov  6  2022 sed-4.9.tar.xz
1441860   2168 -rw-r--r-- 1 root root   2219972 Feb 12 01:15 setuptools-69.1.0.tar.gz
1441861   1768 -rw-r--r-- 1 root root   1806416 Feb 13 18:11 shadow-4.14.5.tar.xz
1441882      8 -rw-r--r-- 1 root root      7324 Feb 29 15:35 systemd-255-upstream_fixes-1.patch
1441862  14516 -rw-r--r-- 1 root root  14861309 Apr 10 07:43 systemd-255.tar.gz
1441863    652 -rw-r--r-- 1 root root    665340 Dec  7 16:57 systemd-man-pages-255.tar.xz
1441864   2264 -rw-r--r-- 1 root root   2317208 Jul 18  2023 tar-1.35.tar.xz
1441866   1168 -rw-r--r-- 1 root root   1192587 Nov 21  2022 tcl8.6.13-html.tar.gz
1441865  10584 -rw-r--r-- 1 root root  10834396 Nov 21  2022 tcl8.6.13-src.tar.gz
1441867   5416 -rw-r--r-- 1 root root   5545720 Oct 18 13:51 texinfo-7.1.tar.xz
1441868    444 -rw-r--r-- 1 root root    451270 Feb  1 18:40 tzdata2024a.tar.gz
1441869   8328 -rw-r--r-- 1 root root   8526168 Dec  4 19:58 util-linux-2.39.3.tar.xz
1441870  17220 -rw-r--r-- 1 root root  17631251 Apr 10 07:44 vim-9.1.0041.tar.gz
1441871    100 -rw-r--r-- 1 root root     98667 Nov 26 14:37 wheel-0.42.0.tar.gz
1441873   1648 -rw-r--r-- 1 root root   1684368 Jan 26 12:19 xz-5.4.6.tar.xz
1441874   1480 -rw-r--r-- 1 root root   1512791 Jan 22 19:53 zlib-1.3.1.tar.gz
1441875   2316 -rw-r--r-- 1 root root   2368543 Apr  4  2023 zstd-1.5.5.tar.gz
```

## Vocabulaire