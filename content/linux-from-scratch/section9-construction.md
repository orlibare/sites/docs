---
title: "Construction #1"
weight: 9
description: "Installation et compilation des logiciels du système de base."
---

## Man-pages-6.06

```
cd /sources
tar -xvf man-pages-6.06.tar.xz
cd man-pages-6.06
```
```
rm -v man3/crypt*
```
```
make prefix=/usr install
```

## Iana-Etc-20240125

```
cd /sources
tar -xvf iana-etc-20240125.tar.gz
cd iana-etc-20240125
```
```
cp services protocols /etc
```

## Glibc-2.39
```
cd $LFS/sources
tar -xvf glibc-2.39.tar.xz
cd glibc-2.39
```
```
patch -Np1 -i ../glibc-2.39-fhs-1.patch
```
```
mkdir -v build
cd build
```
```
../configure --prefix=/usr                   \
             --disable-werror                \
             --enable-kernel=4.19            \
             --enable-stack-protector=strong \
             --disable-nscd                  \
             libc_cv_slibdir=/usr/lib
```
```
make
make check
touch /etc/ld.so.conf
sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
make install
sed '/RTLDLIST=/s@/usr@@g' -i /usr/bin/ldd
```
```
mkdir -pv /usr/lib/locale
localedef -i en_US -f UTF-8 en_US.UTF-8
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
```
```
cat > /etc/nsswitch.conf << EOF
# Début de /etc/nsswitch.conf

passwd: files systemd
group: files systemd
shadow: files systemd

hosts: mymachines resolve [!UNAVAIL=return] files myhostname dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# Fin de /etc/nsswitch.conf
EOF
```
```
tar -xf ../../tzdata2024a.tar.gz
```
```
ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}
```
```
for tz in etcetera southamerica northamerica europe africa antarctica  \
        asia australasia backward; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done
```
```
cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO
```
```
tzselect
```
```
ln -sfv /usr/share/zoneinfo/Europe/Paris> /etc/localtime
```
```
cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib

EOF
```
```
cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
```
```
mkdir -pv /etc/ld.so.conf.d
```
```
cd $LFS/sources
rm -rf glibc-2.39
```
