---
title: "Partitionnement"
weight: 2
description: "Nettoyage, découpage et préparation du disque dur."
---

## Déroulement

### Repérage du disque

```
lsblk | grep disk
```
```
sda     8:0    0   80G  0 disk
```

On a repéré que notre disque était __sda__. On va définir la variable __$hardDisk__ qui reprend l'emplacement et le nom du disque.
```
hardDisk="/dev/"`lsblk | grep disk | cut -f 1 -d " "`
```
On peut vérifier notre variable avec la commande `echo $hardDisk` qui donne __/dev/sda__ en sortie.

### Mise à zéro du disque

À effectuer dans le cas d' un disque ayant déjà été utilisé. 
```
sudo shred -n 1 -v $hardDisk
sudo sgdisk -og $hardDisk
```

### Création des partitions

```
phyMem=`expr $(free -g |awk '/^Mem:/{print $2}') + 1`
```

```
sudo sgdisk -n 1::+256M -t 1:ef00 -c 1:"UEFI boot" $hardDisk
sudo sgdisk -n 2::+${phyMem}G -t 2:8200 -c 2:"Swap" $hardDisk
sudo sgdisk -n 3::+1G -t 3:8300 -c 3:"System boot" $hardDisk
sudo sgdisk -n 4::+30G -t 4:8300 -c 4:"System root" $hardDisk
sudo sgdisk -n 5 -t 5:8300 -c 5:"System home" $hardDisk
```

```
sudo sgdisk -p $hardDisk
```
```
Disk /dev/sda: 167772160 sectors, 80.0 GiB
Model: QEMU HARDDISK
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 02FF968D-468A-4BEC-B2A9-2E45BB40511C
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 167772126
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048          526335   256.0 MiB   EF00  UEFI boot
   2          526336        17303551   8.0 GiB     8200  Swap
   3        17303552        19400703   1024.0 MiB  8300  System boot
   4        19400704        82315263   30.0 GiB    8300  System root
   5        82315264       167772126   40.7 GiB    8300  System home
```

### Formatage des partitions

```
sudo mkfs.vfat ${hardDisk}1 -n "EFI"
sudo mkswap ${hardDisk}2 -L "swap"
sudo swapon ${hardDisk}2
sudo mkfs.ext2 ${hardDisk}3 -L "boot"
sudo mkfs.ext4 ${hardDisk}4 -L "root"
sudo mkfs.ext4 ${hardDisk}5 -L "home"
```

```
sudo blkid | grep $hardDisk | sort
```
```
/dev/sda1: SEC_TYPE="msdos" LABEL_FATBOOT="EFI" LABEL="EFI" UUID="5B6E-748F" BLOCK_SIZE="512" TYPE="vfat" PARTLABEL="UEFI boot" PARTUUID="0b13186e-5aa6-46f0-8394-dcf35a451e49"
/dev/sda2: LABEL="swap" UUID="868c7204-7166-497e-b968-0597450914f3" TYPE="swap" PARTLABEL="Swap" PARTUUID="c5685047-6976-416d-b383-005fc3523828"
/dev/sda3: LABEL="boot" UUID="752b16c3-236a-4e8f-ac7e-7eaa98cd5716" BLOCK_SIZE="4096" TYPE="ext2" PARTLABEL="System boot" PARTUUID="836522c0-607b-44ea-80cc-fdb3fca507b2"
/dev/sda4: LABEL="root" UUID="f1ddb320-561a-43cf-8955-2ec866f9f3a4" BLOCK_SIZE="4096" TYPE="ext4" PARTLABEL="System root" PARTUUID="341c6182-33c2-4363-a10f-3bff9336fe75"
/dev/sda5: LABEL="home" UUID="a6caba64-7f48-4dbd-8bd3-1c10cc9238bf" BLOCK_SIZE="4096" TYPE="ext4" PARTLABEL="System home" PARTUUID="f974e713-eee9-44cc-9dff-c8c4500536d4"
```

### Montage des partitions

```
LFS="/mnt/lfs"
```

```
sudo mkdir $LFS
sudo mount ${hardDisk}4 $LFS
sudo mkdir $LFS/{boot,home}
sudo mount ${hardDisk}3 $LFS/boot
sudo mkdir $LFS/boot/efi
sudo mount ${hardDisk}1 $LFS/boot/efi
sudo mount ${hardDisk}5 $LFS/home
```

```
df -h | grep lfs | sort
```
```
/dev/sda1       256M     0  256M   0% /mnt/lfs/boot/efi
/dev/sda3      1007M   28K  956M   1% /mnt/lfs/boot
/dev/sda4        30G   32K   28G   1% /mnt/lfs
/dev/sda5        40G   24K   38G   1% /mnt/lfs/home
```

```
lsblk
```
```
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
loop0    7:0    0  2.5G  1 loop /usr/lib/live/mount/rootfs/filesystem.squashfs
                                /run/live/rootfs/filesystem.squashfs
sda      8:0    0   80G  0 disk
|-sda1   8:1    0  256M  0 part /mnt/lfs/boot/efi
|-sda2   8:2    0    8G  0 part [SWAP]
|-sda3   8:3    0    1G  0 part /mnt/lfs/boot
|-sda4   8:4    0   30G  0 part /mnt/lfs
`-sda5   8:5    0 40.7G  0 part /mnt/lfs/home
sr0     11:0    1    3G  0 rom  /usr/lib/live/mount/medium
                                /run/live/medium
```



## Vocabulaire