+++
title = "www.orlibare.fr"
description = "Site principal accessible sur https://www.orlibare.fr"
weight = 1
+++


## Création du projet sur framagit

On créé un projet sous https://framagit.org/orlibare/ nommé _www_. Le projet doit avoir les caractéristiques suivantes :
- être vide ;
- être en acces public ;

On se positionne dans le dossier _orlibare/sites_ puis on effectue les actions suivantes :
- Nouveau projet
- Créer un projet vide :
  - Nom du projet : www
  - URL du projet : https://framagit.org/orlibare/sites
  - Identifiant « slug » du projet : www
  - Niveau de visibilité : public
  - Configuration du projet :
    - [ ] Initialiser le dépôt avec un README
    - [ ] Activer les tests statiques de sécurité des applications (SAST) 
- Créer le projet

On s'assure que l'échange de clés SSH fonctionne.


On s'assure que _git_ et _hugo_ soient installés. Si ce n'est pas le cas :



## Création du projet local

### Initialisation

```
mkdir -p ~/Projets/Hugo/Sites/
cd ~/Projets/Hugo/Sites/
```


```
hugo new site www
cd www
```

```
git submodule add https://github.com/lmarcini/hugo-theme-ananke.git themes/hugo-theme-ananke
```
```
tee hugo.toml << EOF
baseURL = '/'
defaultContentLanguage = "fr"
languageCode = 'fr'
title = 'Orlibare'
theme = "hugo-theme-ananke"

[outputs]

[params]
author.name = "Laurent Marciniszyn"
description = "Ordis Libre de l'Arrée"
EOF
```

### Vérification

```
hugo server
```

On vérifie que notre site fonctionne en local via [http://localhost:1313/](http://localhost:1313/)


## Envoi sur du projet sur framagit

```
cd ~/Projets/Hugo/Sites/www
git init --initial-branch=main
git remote add origin git@framagit.org:orlibare/sites/www.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

## Initialisation des framapages


```
tee .gitlab-ci.yml << EOF
image: registry.gitlab.com/pages/hugo/hugo_extended:0.121.0

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  before_script:
  - apk update && apk add git
  script:
  - hugo
  artifacts:
      paths:
      - public
EOF
```

```
git add .
git commit -m "Add/update files"
git push
```

## Association au domaine




