+++
title = "Via debootstrap"
description = "Installation de Debian Linux 10,11 ou 12 en mode UEFI ou en mode BIOS"
weight = 2
+++



## Téléchargement du LiveCD 

On télécharge le LiveCD grml-2022.11 à partir de https://download.grml.org/grml64-full_2022.11.iso et on le grave sur une clé USB avec la commande `dd bs=4M status=progress conv=fdatasync if=grml64-full_2022.11.iso of=/dev/USB_KEY`. 


## Démarrage sur le Live CD

On choisit de démarrer la machine à installer sur la clé USB. Une fois le démarrage effectué, il y a quelques manipulations à effectuer pour pouvoir effectuer l'installation de la machine à partir d'un autre poste de travail pour des raisons de praticité, notamment pour les "copier/coller" des commandes de ce tutoriel.

Les manipulations sont les suivantes :
- __Récupération de l'adresse IP :__ on note l'adresse IP (la première dans la section _Network information_) ;
- __Définition du mot de passe root :__ on entre la comande `passwd` et on saisit un mot de passe temporaire (2 fois) ;
- __Activation de l'accès à distance :__ on entre la commande `systemctl start ssh`.

On se connecte ensuite via la commande `ssh root@adresse_ip` à partir d'un autre poste de travail.

## Environnement

On virtualise le terminal distant et on utilise le shell Bash

- **Input**
```
screen
bash
```

## Modes BIOS/UEFI

Il est important de savoir si notre matériel fonctionne en mode BIOS ou en mode UEFI. Pour cela, on utilise la commande `efibootmanager`

{{< tabs title="Mode" >}}
{{% tab title="BIOS" %}}
- **Input**
```
efibootmgr
```
- **Output**
```
EFI variables are not supported on this system.
```
{{% /tab %}}
{{% tab title="UEFI" %}}
- **Input**
```
efibootmgr
```
- **Output**
```
BootCurrent: 0001
Timeout: 3 seconds
BootOrder: 0001,0002,0008,0000,0003
Boot0000* UiApp
Boot0001* UEFI QEMU DVD-ROM QM00001
Boot0002* UEFI QEMU QEMU HARDDISK
Boot0003* EFI Internal Shell
```
{{% /tab %}}
{{< /tabs >}}

## Mise à zéro du disque système

Il est important de savoir si notre disque est de type SATA ou NVMe avant d'utiliser les commandes `shred` (mise à zéro du disque dur) et `sgdisk` (initialisation d'une nouvelle table de partition au format GPT).

{{< tabs title="Disque" >}}
{{% tab title="SATA" %}}
- **Input**
```
lshw -class disk -short | grep -v '/dev/cdrom'
```
- **Output**
```
H/W path               Device      Class          Description
=============================================================
/0/100/1e/4/1/0/0.0.0  /dev/sda    disk           137GB QEMU HARDDISK
```
- **Input**
```
hardDisk="sda"
hardiskPart="sda"
```
{{% /tab %}}
{{% tab title="NVMe" %}}
- **Input**
```
lshw -class disk -short | grep -v '/dev/cdrom'
```
- **Output**
```
H/W path               Device          Class          Description
=================================================================  
/0/100/1e/4/1/0/0.0.0  /dev/nvme0n1    disk           137GB QEMU HARDDISK
```
- **Input**
```
hardDisk="nvme0n1"
hardiskPart="nvme0n1p"
```
{{% /tab %}}
{{< /tabs >}}

- **Input**
```
shred -v -n 1 /dev/$hardDisk
sgdisk -og /dev/$hardDisk
```

## Initialisation des variables

- **Input**
```
export LANG=C                    #
distro="bookworm"                # choix de la distribution Debian 10/11/12 : buster/bullseye/bookworm 
userName="jdoe"                  # login système de l'utilisateur
userId=10001                     # uid système de l'utilisateur
userInformation="John Doe"       # alias "littéraire" de l'utilisateur - informatif
userPassword="userOrlibare"      # mot de passe de l'utilisateur - choix libre
rootPassword="rootOrlibare"      # mot de passe du super administrateur - choix libre
hostName="pc-orlibare"           # nom de la machine - choix libre
domainName="orlibare.lan"        # nom de domaine - choix libre - garder .lan si machine non exposée directement sur internet
xkbLayout="fr"                   #
xkbModel="pc105"                 #
xkbVariant=""                    #
xkbOptions=""                    #
lvmDisk="system"                 #
ethernetCard="eth0"              #
```

## Partitionnement


{{< tabs title="Mode" >}}
{{% tab title="BIOS" %}}
- **Input**
```
sgdisk -n 1::+256M -t 1:ef01 -c 1:"BIOS boot" /dev/$hardDisk
sgdisk -n 2::+1G -t 2:8300 -c 2:"System boot" /dev/$hardDisk
sgdisk -n 3 -t 3:8e00 -c 3:"System volumes" /dev/$hardDisk
```
{{% /tab %}}
{{% tab title="UEFI" %}}
- **Input**
```
sgdisk -n 1::+256M -t 1:ef00 -c 1:"UEFI boot" /dev/$hardDisk
sgdisk -n 2::+1G -t 2:8300 -c 2:"System boot" /dev/$hardDisk
sgdisk -n 3 -t 3:8e00 -c 3:"System volumes" /dev/$hardDisk
```
{{% /tab %}}
{{< /tabs >}}

__Vérification__

{{< tabs title="Mode" >}}
{{% tab title="BIOS" %}}
- **Input**
```
sgdisk -p /dev/$hardDisk
```
- **Output**
```
Disk /dev/sda: 268435456 sectors, 128.0 GiB
Model: QEMU HARDDISK
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): FC52C77E-C14D-46AA-8286-32E5347EF482
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 268435422
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048          526335   256.0 MiB   EF01  BIOS boot
   2          526336         2623487   1024.0 MiB  8300  System boot
   3         2623488       268435422   126.7 GiB   8E00  System volumes
```
{{% /tab %}}
{{% tab title="UEFI" %}}
- **Input**
```
sgdisk -p /dev/$hardDisk
```
- **Output**
```
Disk /dev/sda: 268435456 sectors, 128.0 GiB
Model: QEMU HARDDISK
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 3E5810B7-F8BA-4DFB-A37E-F8475CFD3FD8
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 268435422
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048          526335   256.0 MiB   EF00  UEFI boot
   2          526336         2623487   1024.0 MiB  8300  System boot
   3         2623488       268435422   126.7 GiB   8E00  System volumes
```
{{% /tab %}}
{{< /tabs >}}

## Volumes logiques

- **Input**
```
pvcreate /dev/${hardDisk}3
vgcreate $lvmDisk /dev/${hardDisk}3
lvcreate -L 4G -n swap $lvmDisk
lvcreate -L 50G -n root $lvmDisk
lvcreate -L 20G -n opt $lvmDisk
lvcreate -L 20G -n usr_local $lvmDisk
lvcreate -L 20G -n var $lvmDisk
lvcreate -L 8G -n var_log $lvmDisk
lvcreate -L 4G -n var_log_audit $lvmDisk
lvcreate -l 100%FREE -n var_tmp $lvmDisk
```

## Formatage

{{< tabs title="Mode" >}}
{{% tab title="BIOS" %}}
- **Input**
```
mkfs.ext2 /dev/${hardDisk}2 -L "boot"
mkfs.ext4 /dev/mapper/$lvmDisk-root -L "root"
mkfs.ext4 /dev/mapper/$lvmDisk-opt -L "opt"
mkfs.ext4 /dev/mapper/$lvmDisk-usr_local -L "usr_local"
mkfs.ext4 /dev/mapper/$lvmDisk-var -L "var"
mkfs.ext4 /dev/mapper/$lvmDisk-var_log -L "var_log"
mkfs.ext4 /dev/mapper/$lvmDisk-var_log_audit -L "var_log_audit"
mkfs.ext4 /dev/mapper/$lvmDisk-var_tmp -L "var_tmp"
mkswap /dev/mapper/$lvmDisk-swap -L "swap"
swapon /dev/mapper/$lvmDisk-swap
```
{{% /tab %}}
{{% tab title="UEFI" %}}
- **Input**
```
mkfs.vfat /dev/${hardDisk}1 -n "EFI"
mkfs.ext2 /dev/${hardDisk}2 -L "boot"
mkfs.ext4 /dev/mapper/$lvmDisk-root -L "root"
mkfs.ext4 /dev/mapper/$lvmDisk-opt -L "opt"
mkfs.ext4 /dev/mapper/$lvmDisk-usr_local -L "usr_local"
mkfs.ext4 /dev/mapper/$lvmDisk-var -L "var"
mkfs.ext4 /dev/mapper/$lvmDisk-var_log -L "var_log"
mkfs.ext4 /dev/mapper/$lvmDisk-var_log_audit -L "var_log_audit"
mkfs.ext4 /dev/mapper/$lvmDisk-var_tmp -L "var_tmp"
mkswap /dev/mapper/$lvmDisk-swap -L "swap"
swapon /dev/mapper/$lvmDisk-swap
```
{{% /tab %}}
{{< /tabs >}}




## Montage des partitions

```
mkdir /install/
mount /dev/mapper/$lvmDisk-root /install
mkdir /install/{boot,opt,var}
mount /dev/${hardDisk}2 /install/boot
mkdir /install/boot/efi
mount /dev/${hardDisk}1 /install/boot/efi
mount /dev/mapper/$lvmDisk-opt /install/opt
mkdir /install/usr/local
mount /dev/mapper/$lvmDisk-usr_local /install/usr/local
mount /dev/mapper/$lvmDisk-var /install/var
mkdir /install/var/{log,tmp}
mount /dev/mapper/$lvmDisk-var_log /install/var/log
mount /dev/mapper/$lvmDisk-var_tmp /install/var/tmp
mkdir /install/var/log/audit
mount /dev/mapper/$lvmDisk-var_log_audit /install/var/log/audit
```

## Installation du système de base

```
debootstrap --arch=amd64 $distro /install/ http://debian.mirrors.ovh.net/debian/
```

## Configuration de /etc/fstab

```
cat << EOF > /install/etc/fstab
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "# <file system>" "<mount point>" "<type>" "<options>" "<dump>" "<pass>"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/${hardDisk}2)\"" "/boot" "ext2" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/${hardDisk}1)\"" "/boot/efi" "vfat" "defaults,noauto" "0" "0"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-root)\"" "/" "ext4" "errors=remount-ro" "0" "1"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-opt)\"" "/opt" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-usr_local)\"" "/usr/local" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-var)\"" "/var" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-var_log)\"" "/var/log" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-var_log_audit)\"" "/var/log/audit" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-var_tmp)\"" "/var/tmp" "ext4" "defaults" "0" "2"`
`printf "%-50s%-20s%-10s%-20s%-10s%-5s\n" "UUID=\"$(blkid -o value -s UUID /dev/mapper/$lvmDisk-swap)\"" "none" "swap" "sw" "0" "0"`
EOF
```

## Configuration de /etc/hostname

```
cat << EOF > /install/etc/hostname
$hostName
EOF
```

## Configuration de /etc/hosts

```
cat << EOF > /install/etc/hosts
`printf "%-20s%-40s%-20s%-5s\n" "127.0.0.1" "localhost.localdomain" "localhost"`
`printf "%-20s%-40s%-30s%-5s\n" "\`hostname -I | cut -d " " -f1\`" "$hostName.$domainName" "$hostName"`
EOF
```

## Configuration du fuseau horaire

```
chroot /install ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

## Configuration des dépôts Debian

```
cat << EOF > /install/etc/apt/sources.list
`printf "%-50s%-25s%-5s\n" "deb http://ftp.fr.debian.org/debian/" "$distro" "main contrib non-free non-free-firmware"`
`printf "%-50s%-25s%-5s\n" "deb http://ftp.fr.debian.org/debian/" "$distro-updates" "main contrib non-free"`
`printf "%-50s%-25s%-5s\n" "deb http://deb.debian.org/debian/" "$distro-backports" "main contrib non-free"`
`printf "%-50s%-25s%-5s\n" "deb http://security.debian.org/" "$distro-security" "main contrib non-free"`
EOF
```

## Montage des ressources

```
mount --bind /dev /install/dev
mount --bind /dev/pts /install/dev/pts
mount --bind /sys /install/sys
mount --bind /run /install/run/
mount -t proc /proc /install/proc
```


## Mise à jour

```
rm -rf /install/etc/resolv.conf
cp /etc/resolv.conf /install/etc/
```

```
chroot /install apt -y update
chroot /install apt -y full-upgrade
```

## Installation du noyau

```
chroot /install apt -y install grub-efi           \
                               linux-image-amd64 \
                               lvm2
```

## Installation des outils complémentaires

```
chroot /install apt -y install console-setup              \
                               curl                       \
                               gdisk                      \
                               gpg                        \
                               git                        \
                               htop                       \
                               ifupdown                   \
                               keyboard-configuration     \
                               locales                    \
                               mc                         \
                               ncdu                       \
                               net-tools                  \
                               screen                     \
                               rsync                      \
                               ssh                        \
                               sudo                       \
                               tree                       \
                               vim                        \
                               wget                       \
                               zip
```

## Configuration du shell

```
sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' /install/etc/skel/.bashrc
```
```
cat << EOF >> /install/etc/skel/.bashrc
if [ \`whoami\` = root ]; then
    PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;31m\]\u@\[\033[01;33m\]\$(hostname)\[\033[01;31m\].\$(hostname -d)\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]# '
else
    PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;32m\]\u@\[\033[01;33m\]\$(hostname)\[\033[01;32m\].\$(hostname -d)\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ '
fi
EOF
```
```
\cp /install/etc/skel/.bashrc /install/root/
```

## Mot de passe administrateur

```
chroot /install echo root:$rootPassword | chroot /install chpasswd -e
```
## Ajout utilisateur 

```
chroot /install useradd -m -G adm,cdrom,dip,games,plugdev,sudo,video -s /bin/bash -d /home/$userName $userName -u $userId -c "$userInformation"
echo "$userName:$userPassword" | chroot /install chpasswd -e
```

## Service rc.local

```
cat << EOF > /install/etc/systemd/system/rc-local.service
[Unit]
Description=/etc/rc.local Compatibility
ConditionPathExists=/etc/rc.local

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99

[Install]
WantedBy=multi-user.target
EOF
```
```
cat << EOF > /install/etc/rc.local
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
exit 0
EOF
```
```
chmod +x /install/etc/rc.local
chroot /install systemctl enable rc-local.service
```

## tmpfs sur /tmp

```
cat << EOF > /install/etc/systemd/system/tmp.mount
[Unit]
Description=Temporary Directory
Before=local-fs.target

[Mount]
What=tmpfs
Where=/tmp
Type=tmpfs
Options=defaults,nosuid,nodev,noexec,size=50% 

[Install]
WantedBy=local-fs.target
EOF
```

```
chroot /install systemctl enable tmp.mount
``` 

## Configuration des locales

```
sed -i 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /install/etc/locale.gen
```
```
cat << EOF > /install/etc/default/locale
LANG=fr_FR.UTF-8
LC_MESSAGES=fr_FR.UTF-8
EOF
```
```
chroot /install dpkg-reconfigure -f noninteractive locales
```

## Configuration du clavier

```
cat << EOF > /install/etc/default/keyboard
XKBMODEL="$xkbModel"
XKBLAYOUT="$xkbLayout"
XKBVARIANT="$xkbVariant"
XKBOPTIONS="$xkbOptions"
BACKSPACE="guess"
EOF
```
```
chroot /install dpkg-reconfigure -f noninteractive keyboard-configuration
```

## Configuration de la console

```
cat << EOF > /install/etc/default/console-setup
ACTIVE_CONSOLES="/dev/tty[1-6]"
CHARMAP="UTF-8"
CODESET="Lat15"
FONTFACE="VGA"
FONTSIZE="8x16"
VIDEOMODE=
EOF
```
```
chroot /install dpkg-reconfigure -f noninteractive console-setup
```

## Configuration du réseau
```
cat << EOF > /install/etc/network/interfaces
auto lo
iface lo inet loopback

auto $ethernetCard
iface $ethernetCard inet dhcp
EOF
```

## Désactivation IPv6

```
cat << EOF > /install/etc/sysctl.d/10-disable-ipv6.conf
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
EOF
```

## Installation du gestionnaire d'amorçage

```
cat << EOF > /install/etc/default/grub
GRUB_DEFAULT=0
GRUB_TIMEOUT_STYLE=hidden
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR=\`lsb_release -i -s 2> /dev/null || echo Debian\`
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX="net.ifnames=0"
EOF
```

```
chroot /install grub-mkconfig -o /boot/grub/grub.cfg
chroot /install grub-install --target=x86_64-efi
chroot /install update-grub
chroot /install update-initramfs -u
```

## Nettoyage et arrêt

```
sudo su -c '> /root/.bash_history'
sudo su -c 'history -cw'
> ~/.bash_history
history -cw
sudo shutdown -h now
```
