---
title: "Environnement"
weight: 4
description: "Préparation de l'environnement nécessaire à la construction du système."
---

## Structure des répertoires

```
sudo mkdir -pv $LFS/{etc,usr/{bin,lib,sbin},var}
```
```
for i in bin lib sbin; do
    sudo ln -sv usr/$i $LFS/$i
done
```
```
case $(uname -m) in
    x86_64) sudo mkdir -pv $LFS/lib64 ;;
esac
```

```
sudo mkdir -pv $LFS/tools
```

## Ajout de l'utilisateur LFS

```
sudo groupadd lfs
sudo useradd -s /bin/bash -g lfs -m -k /dev/null lfs
```

```
echo "lfs:lfs" | sudo chpasswd
```

## Droits sur /mnt/lfs

```
sudo chown -v lfs $LFS/{usr{,/*},lib,var,etc,bin,sbin,tools}
case $(uname -m) in
    x86_64) sudo chown -v lfs $LFS/lib64 ;;
esac
```

## Connexion utilisateur

```
su - lfs
```

## Environnement utilisateur

### Création de ~/.bash_profile

```
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF
```

### Création de ~/.bashrc

```
cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/usr/bin
if [ ! -L /bin ]; then PATH=/bin:$PATH; fi
PATH=$LFS/tools/bin:$PATH
CONFIG_SITE=$LFS/usr/share/config.site
export LFS LC_ALL LFS_TGT PATH CONFIG_SITE
EOF
```

### Désactivation de /etc/bash.bashrc

```
su - user -c "sudo mv -v /etc/bash.bashrc /etc/bash.bashrc.NOUSE"
```

### Ajustement des coeurs pour compilation

```
cat >> ~/.bashrc << "EOF"
export MAKEFLAGS=-j$(nproc)
EOF
```
```
source ~/.bash_profile
```
