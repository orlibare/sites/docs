+++
title = "docs.orlibare.fr"
description = "Site documentaire accessible sur https://documentation.orlibare.fr"
weight = 2
+++


```
cd existing_folder
git init --initial-branch=main
git remote add origin git@framagit.org:orlibare/documentation.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

```
git submodule add https://github.com/lmarcini/hugo-theme-relearn.git themes/hugo-theme-relearn
```
```
baseURL = '/'
defaultContentLanguage = "fr"
languageCode = 'fr'
title = 'doc.orilbare.fr'
theme = "hugo-theme-relearn"

[outputs]
  home = ["HTML", "RSS", "PRINT", "SEARCH"]
  section = ["HTML", "RSS", "PRINT"]
  page = ["HTML", "RSS", "PRINT"]

[params]
author.name = "Laurent Marciniszyn"
description = "Documentation personnelle"
themeVariant =  "blue"
```
